

class clockInRecord {
    data = [
        {
            name: '马小朋',
            workCome_signIn: '迟到',
            workLeave_signIn: '正常'
        },
        {
            name: '马小朋',
            workCome_signIn: '正常',
            workLeave_signIn: '早退'
        },
        {
            name: '马小朋',
            workCome_signIn: '正常',
            workLeave_signIn: '请假'
        },
        {
            name: '马小朋',
            workCome_signIn: '正常',
            workLeave_signIn: '正常'
        },
        {
            name: '马小朋',
            workCome_signIn: '迟到',
            workLeave_signIn: '正常'
        },
        {
            name: '马小朋',
            workCome_signIn: '正常',
            workLeave_signIn: '请假'
        },
        {
            name: '马小朋',
            workCome_signIn: '正常',
            workLeave_signIn: '早退'
        },
        {
            name: '马小朋',
            workCome_signIn: '正常',
            workLeave_signIn: '正常'
        },
        {
            name: '马小朋',
            workCome_signIn: '迟到',
            workLeave_signIn: '正常'
        },
        {
            name: '马小朋',
            workCome_signIn: '请假',
            workLeave_signIn: '正常'
        },
        {
            name: '马小朋',
            workCome_signIn: '正常',
            workLeave_signIn: '早退'
        },
        {
            name: '马小朋',
            workCome_signIn: '正常',
            workLeave_signIn: '正常'
        },
        {
            name: '马小朋',
            workCome_signIn: '正常',
            workLeave_signIn: '早退'
        },
        {
            name: '马小朋',
            workCome_signIn: '正常',
            workLeave_signIn: '正常'
        }
    ];
    constructor(clockInRecord) {
        this.clockInRecordEl = document.querySelector(clockInRecord);//获取底层box
        this.selectEl = this.clockInRecordEl.querySelector('.dataTable_length');//获取选择下拉框
        this.tableWillChangeEl = this.clockInRecordEl.querySelector('.change');//获取表格变动情况
        this.pageSumEl = this.clockInRecordEl.querySelector('.pageSum');//获取页数情况
        this.pageChangeEl = this.clockInRecordEl.querySelector('.tableAfter_row');
        this.inputValueEl = this.clockInRecordEl.querySelector('.tableAfter_row .col_right')

        this.selectOptionIndex = 2;
        this.selectOptionText = 6;
        this.currentPage = 1;
        // this.pageSumElChildren = this.pageSumEl.children;

        this.bindEvent();
        this.getSelectOnchange();
        this.addTableWillChange(this.selectOptionText);
        this.addPageSUm(this.selectOptionText);



    }

    //--获取选择下拉框的 option 的text。就是每页最多显示多少行。
    getSelectOnchange() {
        this.selectEl.onchange = (() => {
            this.selectOptionIndex = this.selectEl.selectedIndex;
            // console.log(this.selectOptionIndex);
            this.getOptionValue();
        })
    }
    getOptionValue() {
        this.selectOptionText = this.selectEl.children[this.selectOptionIndex].text * 1;
        // console.log(this.selectOptionText);
        this.tableWillChangeEl.innerHTML = '';
        this.pageSumEl.innerHTML = '';
        this.addPageSUm(this.selectOptionText);
        this.bindEvent();

    }


    //--增加willChange
    addTableWillChange(selectOptionText) {
        if ((this.data.length - (this.currentPage - 1) * selectOptionText) > selectOptionText) {
            this.willChange((this.currentPage - 1) * selectOptionText, this.currentPage * selectOptionText)
        } else {
            this.willChange((this.currentPage - 1) * selectOptionText, this.data.length);
        }
    }
    willChange(dataIndex0, dataIndex1) {
        let newDiv = '';
        for (let i = dataIndex0; i < dataIndex1; i++) {
            newDiv = newDiv + `<div class='willChange'>
            <li><span>${this.data[i].name}&nbsp;</span></li>
            <li>${this.data[i].workCome_signIn}</li>
            <li>${this.data[i].workLeave_signIn}</li>
            </div>`
        }
        this.tableWillChangeEl.innerHTML = newDiv;
    }


    //--增加页数
    addPageSUm(selectOptionText) {
        let page = Math.ceil(this.data.length / selectOptionText);
        let newButton = ''
        for (let i = 1; i < page + 1; i++) {
            newButton = newButton + `<button class="page">${i}</button>`
        }
        this.pageSumEl.innerHTML = newButton;
        this.pageSumEl.children[0].classList.add('active')
    }

    bindEvent() {
        // console.log('运行了');
        this.currentPage = 1;
        this.addTableWillChange(this.selectOptionText);

        // let array01 = Array.from(this.pageSumElChildren)//----伪数组记得转化为数组
        //直接事件委托
        this.pageChangeEl.onclick = (e) => {
            this.pageClick(e.target);
            this.addTableWillChange(this.selectOptionText);
        }

    }
    // pageTo(){}
    pageClick(ETarget) {
        if (ETarget.matches('.pageTo')) {
            this.inputValueEl.value = prompt('输入value')//----------这里改成一个键盘时间就可以了，也就是监听到点击事件后 再搞一个键盘事件
            this.currentPage = this.inputValueEl.value;
            for (let i = 0; i < Math.ceil(this.data.length / this.selectOptionText); i++) {
                this.pageSumEl.children[i].classList.remove('active');
            }
            this.pageSumEl.children[this.inputValueEl.value - 1].classList.add('active');
        }

        else if (ETarget.matches('.home_page')) {
            this.currentPage = 1;
            for (let i = 0; i < Math.ceil(this.data.length / this.selectOptionText); i++) {
                this.pageSumEl.children[i].classList.remove('active');
            }
            this.pageSumEl.children[0].classList.add('active');
        }
        else if (ETarget.matches('.trailer_page')) {
            this.currentPage = Math.ceil(this.data.length / this.selectOptionText)
            for (let i = 0; i < Math.ceil(this.data.length / this.selectOptionText); i++) {
                this.pageSumEl.children[i].classList.remove('active');
            }
            this.pageSumEl.children[Math.ceil(this.data.length / this.selectOptionText) - 1].classList.add('active');
        }
        else if (ETarget.matches('.page')) {
            this.currentPage = ETarget.textContent
            for (let i = 0; i < Math.ceil(this.data.length / this.selectOptionText); i++) {
                this.pageSumEl.children[i].classList.remove('active');
            }
            this.pageSumEl.children[this.currentPage - 1].classList.add('active');
        }
        else if (ETarget.matches('.next') && (this.currentPage * 1 + 1) <= Math.ceil(this.data.length / this.selectOptionText)) {
            this.currentPage = this.currentPage * 1 + 1;
            this.pageSumEl.children[this.currentPage - 2].classList.remove('active');
            this.pageSumEl.children[this.currentPage - 1].classList.add('active');

        }
        else if (ETarget.matches('.prev') && (this.currentPage * 1 - 1) > 0) {
            this.currentPage = this.currentPage * 1 - 1;
            this.pageSumEl.children[this.currentPage].classList.remove('active');
            this.pageSumEl.children[this.currentPage - 1].classList.add('active');
        }
    }
}
let record = new clockInRecord('.clockInRecord');
console.log(record);



class sidebar {
    constructor(sidebar, clockInRecord) {
        this.sidebarEl = document.querySelector(sidebar);//--------获取侧边栏
        this.clockInRecordEl = document.querySelector(clockInRecord);//-----获取正文表区
        // this.s = this.clockInRecordEl.getBoundingClientRect();
        // this.left = Math.ceil(this.clockInRecordEl.getBoundingClientRect().left);
        this.roundBoxEl = document.querySelector('.sidebar .roundBoxL');//------获取侧边栏圆圈
        this.top_searchEl = document.querySelector('.top_search');//------获取顶层栏的搜索区
        this.registerEl = document.querySelector('.sidebar .register');
        this.bindEvent();
    }
    bindEvent() {
        this.roundBoxEl.onclick = () => {
            this.sidebarEl.classList.toggle('active');
            this.roundBoxEl.textContent = this.roundBoxEl.textContent == ">" ? "<" : ">";
            this.clockInRecordEl.classList.toggle('active');
            this.top_searchEl.classList.toggle('active');
        //     this.registerEl.innerHTML = this.registerEl.innerHTML == `'<svg t="1652691390746" class="icon" viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg"
        //     p-id="6089" width="25" height="25">
        //     <path
        //         d="M575.215 547.318c53.367-24.316 90.562-78.011 90.562-140.522 0-85.257-69.149-154.383-154.406-154.383-85.299 0-154.427 69.126-154.427 154.383 0 62.49 37.172 116.185 90.562 140.522-87.156 27.24-150.586 108.698-150.586 204.715 0 12.071 9.779 21.827 21.827 21.827s21.827-9.756 21.827-21.827c0-94.161 76.613-170.774 170.776-170.774 94.184 0 170.797 76.613 170.797 170.774 0 12.071 9.756 21.827 21.827 21.827 12.07 0 21.827-9.756 21.827-21.827 0.021-95.994-63.43-177.475-150.586-204.715zM400.621 406.817c0-61.072 49.678-110.729 110.773-110.729 61.072 0 110.75 49.657 110.75 110.729 0 61.094-49.678 110.794-110.75 110.794-61.095 0-110.773-49.7-110.773-110.794z"
        //         p-id="6090" fill="#e6e6e6"></path>
        //     <path
        //         d="M511.371 960.81c-246.951 0-447.869-200.918-447.869-447.891 0-246.93 200.919-447.871 447.869-447.871 246.973 0 447.892 200.919 447.892 447.871 0 246.973-200.919 447.891-447.892 447.891z m0-854.269c-224.098 0-406.398 182.301-406.398 406.377s182.3 406.397 406.398 406.397c224.099 0 406.42-182.321 406.42-406.397S735.47 106.541 511.371 106.541z"
        //         p-id="6091" fill="#e6e6e6"></path>
        // </svg>'` ? `<svg t="1652691390746" class="icon" viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg"
        // p-id="6089" width="25" height="25">
        // <path
        //     d="M575.215 547.318c53.367-24.316 90.562-78.011 90.562-140.522 0-85.257-69.149-154.383-154.406-154.383-85.299 0-154.427 69.126-154.427 154.383 0 62.49 37.172 116.185 90.562 140.522-87.156 27.24-150.586 108.698-150.586 204.715 0 12.071 9.779 21.827 21.827 21.827s21.827-9.756 21.827-21.827c0-94.161 76.613-170.774 170.776-170.774 94.184 0 170.797 76.613 170.797 170.774 0 12.071 9.756 21.827 21.827 21.827 12.07 0 21.827-9.756 21.827-21.827 0.021-95.994-63.43-177.475-150.586-204.715zM400.621 406.817c0-61.072 49.678-110.729 110.773-110.729 61.072 0 110.75 49.657 110.75 110.729 0 61.094-49.678 110.794-110.75 110.794-61.095 0-110.773-49.7-110.773-110.794z"
        //     p-id="6090" fill="#e6e6e6"></path>
        // <path
        //     d="M511.371 960.81c-246.951 0-447.869-200.918-447.869-447.891 0-246.93 200.919-447.871 447.869-447.871 246.973 0 447.892 200.919 447.892 447.871 0 246.973-200.919 447.891-447.892 447.891z m0-854.269c-224.098 0-406.398 182.301-406.398 406.377s182.3 406.397 406.398 406.397c224.099 0 406.42-182.321 406.42-406.397S735.47 106.541 511.371 106.541z"
        //     p-id="6091" fill="#e6e6e6"></path>
        // </svg>
        // <div class="left">登录</div>
        // <div class="mid"></div>
        // <div class="right">注册</div>` : `<svg t="1652691390746" class="icon" viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg"
        // p-id="6089" width="25" height="25">
        // <path
        // d="M575.215 547.318c53.367-24.316 90.562-78.011 90.562-140.522 0-85.257-69.149-154.383-154.406-154.383-85.299 0-154.427 69.126-154.427 154.383 0 62.49 37.172 116.185 90.562 140.522-87.156 27.24-150.586 108.698-150.586 204.715 0 12.071 9.779 21.827 21.827 21.827s21.827-9.756 21.827-21.827c0-94.161 76.613-170.774 170.776-170.774 94.184 0 170.797 76.613 170.797 170.774 0 12.071 9.756 21.827 21.827 21.827 12.07 0 21.827-9.756 21.827-21.827 0.021-95.994-63.43-177.475-150.586-204.715zM400.621 406.817c0-61.072 49.678-110.729 110.773-110.729 61.072 0 110.75 49.657 110.75 110.729 0 61.094-49.678 110.794-110.75 110.794-61.095 0-110.773-49.7-110.773-110.794z"
        // p-id="6090" fill="#e6e6e6"></path>
        //     <path
        //         d="M511.371 960.81c-246.951 0-447.869-200.918-447.869-447.891 0-246.93 200.919-447.871 447.869-447.871 246.973 0 447.892 200.919 447.892 447.871 0 246.973-200.919 447.891-447.892 447.891z m0-854.269c-224.098 0-406.398 182.301-406.398 406.377s182.3 406.397 406.398 406.397c224.099 0 406.42-182.321 406.42-406.397S735.47 106.541 511.371 106.541z"
        //         p-id="6091" fill="#e6e6e6"></path>
        // </svg> `;
        }
    }
}
let side = new sidebar('.sidebar', '.clockInRecord', 'top');
console.log(side);
