let btnEl = document.querySelector('.content>button');
let textEl = document.querySelector('.text');
let usernameEl = document.querySelector('.user input');
let passwordEl = document.querySelector('.password input');
let emailEl = document.querySelector('.email input');
let codeEl = document.querySelector('.code input');
let codeBtnEl = document.querySelector('.code button');

btnEl.onclick = async function() {
    let username = usernameEl.value;
    let password = passwordEl.value;
    let email = emailEl.value;
    let code = codeEl.value;

    if (!username || !password) {
        alert("用户名或密码未填写");
        return;
    }

    if (!code) {
        alert("未输验证码");
        return;
    }

    let res = await axios({
        url: "http://localhost:8080/register",
        method: "post",
        data: {
            username: username,
            password: password,
            email: email,
            code: code
        }
    })

    console.log(res);

    let data = res.data;

    if (data.code !== 200) {
        return alert(data.msg)
    }

    

    localStorage.user = JSON.stringify(data.data);
    location.href = '../../html/lj/login.html';
}

// 点击发送按钮 button禁止
codeBtnEl.onclick = async function() {
    var time = 60;
    let email = emailEl.value;

    codeBtnEl.disabled = true;
    codeBtnEl.classList.add('active');
    var timer = setInterval(function() {
        if (time == 0) {
            clearInterval(timer);
            codeBtnEl.disabled = false;
            codeBtnEl.classList.remove('active');
            codeBtnEl.innerHTML = '发送';
            time = 60;
        } else {
            codeBtnEl.innerHTML = '还剩下' + time + '秒';
            time--;
        }
    }, 1000);

    let re = await axios({
        url: "http://localhost:8080/sendEmails",
        method: "post",
        data: {
            email: email,
            page: 200
        }
    })

    let datas = re.data;
    console.log(datas);
}

// 点击最下方文字 跳转到登录界面
textEl.onclick = function() {
    location.href = '../../html/lj/login.html';
}