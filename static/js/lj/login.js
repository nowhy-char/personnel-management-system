let btnEl = document.querySelector('button');
let textEl = document.querySelector('.text');
let usernameEl = document.querySelector('.user input');
let passwordEl = document.querySelector('.password input');

btnEl.onclick = async function() {
    let username = usernameEl.value;
    let password = passwordEl.value;

    if (!username || !password) {
        alert("用户名或密码未填写");
        return;
    }

    let res = await axios({
        url: "http://localhost:8080/login",
        method: "post",
        data: {
            username: username,
            password: password
        }
    })

    console.log(res);

    let data = res.data;

    if (data.code !== 200) {
        return alert(data.msg);
    }

    localStorage.user = JSON.stringify(data.data);
    location.href = '../../html/yl/jixiaokaohebiao.html';
}

textEl.onclick = function() {
    location.href = '../../html/lj/register.html';
}