let btnEl = document.querySelector('button');
// let textEl = document.querySelector('.text');
let usernameEl = document.querySelector('.user input');

btnEl.onclick = async function() {
    let username = usernameEl.value;

    if (!username) {
        alert("你未填写员工编号");
        return;
    }

    let res = await axios({
        url: "/",
        method: "post",
        data: {
            username: username,
        }
    })

    let data = res.data;

    if (data.code !== 200) {
        return alert(data.msg);
    }

    localStorage.user = JSON.stringify(data.data);
    location.href = '../../html/pw/clock.html';
}
// textEl.onclick = function() {
//     location.href = '../../html/lj/register.html';
// }