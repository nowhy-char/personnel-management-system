let headItemList = document.querySelectorAll(".head .item");
let contentItemList = document.querySelectorAll(".content .item");
let currentIndex = 0;
let onboarding = document.querySelector(".onboarding");
let resignation = document.querySelector(".resignation");
let onboardingClick = false;
let resignationClick = false;

headItemList.forEach(function (headItem, index) {
  headItem.i = index;

  headItem.onclick = function () {
    // 从点击元素上获取对应的位置信息
    let index = this.i;

    // 清除上一次的状态
    headItemList[currentIndex].classList.remove("active");
    contentItemList[currentIndex].classList.remove("active");

    // 给当前点击的内容添加状态
    headItemList[index].classList.add("active");
    contentItemList[index].classList.add("active");

    currentIndex = index;
  };
});
onboarding.onclick = async function () {
  onboardingClick = true;
  clickListen(onboardingClick, resignationClick);
  let html = "";
  html += `
    <div class="container">
        <table cellspacing="0">
        <thead>
          <tr>
            <th colspan="4">入职办理登记</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>姓名：<input type="text" id="name"></td>
            <td>年龄：<input type="text" id="age"></td>
            <td>学历：<input type="text" id="edu"></td>
            <td>邮箱：<input type="text" id="email"></td>
          </tr>
          <tr>
            <td colspan="2">家庭住址：<input type="text" id="address"></td>
            <td>身份证号：<input type="text" id="card"></td>
            <td>联系方式：<input type="text" id="phone"></td>
          </tr>
          <tr>
            <td>入职时间：<input type="text" id="startTime"></td>
            <td>任职岗位：<input type="text" id="job"></td>
          </tr>
        </tbody>
      </table>
      
    <div>
    <div class="complete">确定</div>
    <div class="exit">返回上一步</div>
    `;
  onboarding.innerHTML = html;
  onboarding.classList.remove("com");
  // console.log(e.target.classList.contains("complete"))
  // resignation.remove()
  // console.log(onboarding.includes());
  // await console.log(e.target.classList.contains("complete"))
  let name = await document.querySelector("#name");
  let age = await document.querySelector("#age");
  let edu = await document.querySelector("#edu");
  let email = await document.querySelector("#email");
  let address = await document.querySelector("#address");
  let card = await document.querySelector("#card");
  let phone = await document.querySelector("#phone");
  let startTime = await document.querySelector("#startTime");
  let job = await document.querySelector("#job");
  let complete = await document.querySelector(".complete");
  let exit = await document.querySelector(".exit");
  complete.onclick = async function () {
    // onboarding.removeChild(document.querySelector(".container"));
    document.querySelector(".container").remove();
    // html ="Enter"
    // onboarding.innerHTML = "html" ;
    // window.history.back(-1);
    // onboarding.textContent ="入职办理"
    // console.log(onboarding.innerHTML)
    // console.log(html)
    let res = await axios({
      url: "http://localhost:8080/setStaffn",
      method: "POST",
      data: {
        s_name: name.value.trim(),
        s_card: card.value.trim(),
        s_age: age.value.trim(),
        s_add: address.value.trim(),
        s_phone: phone.value.trim(),
        s_edu: edu.value.trim(),
        s_position: job.value.trim(),
      },
    });
    // let {code,msg,data}=res.data;
    let data = res.data;
    if (data.code !== 200) {
      return alert(data.msg);
    }

    localStorage.staffn = JSON.stringify(data.data);
  };
  
  exit.onclick = function () {
    // document.querySelector(".container").remove();
  };
};

function clickListen(onboardingClick, resignationClick) {
  if (onboardingClick == true) {
    return onboarding.nextElementSibling.style.display="none";
  }
  if (resignationClick == true) {
    return resignation.previousElementSibling.style.display="none";
  }
}
resignation.onclick = async function () {
  resignationClick = true;
  clickListen(onboardingClick, resignationClick);
  let html = "";
  html += `
    <table cellspacing="0">
      <thead>
        <tr>
          <th colspan="4">离职办理登记</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td>姓名：<input type="text" id="name"></td>
          <td>年龄：<input type="text" id="age"></td>
          <td>离职原因：<input type="text" id="reason"></td>
          <td>离职时间：<input type="text" id="endTime"></td>
        </tr>
      </tbody>
    </table>
    <div class="complete">确定</div>

    <div class="exit">返回上一步</div>
    `;
  resignation.outerHTML = html;
  resignation.classList.remove("com");
  let name = await document.querySelector("#name");
  let age = await document.querySelector("#age");
  let reason = await document.querySelector("#reason");
  let endTime = await document.querySelector("#endTime");
  let complete = await document.querySelector(".complete");
  let exit = await document.querySelector(".exit");
  complete.onclick = async function () {
    // onboarding.removeChild(document.querySelector(".container"));
    // html ="Enter"
    // onboarding.innerHTML = "html" ;
    // window.history.back(-1);
    // onboarding.textContent ="入职办理"
    // console.log(onboarding.innerHTML)
    // console.log(html)
    let res = await axios({
      url: "http://localhost:8080/setStaffn ",
      method: "POST",
      data: {
        s_name: name.value.trim(),
        s_reason: reason.value.trim(),
        s_age: age.value.trim(),
        s_endTime: endTime.value.trim(),
      },
    });
    let data = res.data;
    if (data.code !== 200) {
      return alert(data.msg);
    }

    localStorage.staffn = JSON.stringify(data.data);
  };
  exit.onclick = function () {
    // document.querySelector(".container").remove();
  };
  
};
// localStorage.staffn = JSON.stringify(data.data);
