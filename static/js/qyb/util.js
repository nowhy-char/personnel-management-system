async function ajax({ method = "GET", url, params = {} } = {}) {
  method = method.toUpperCase();
  let res = await axios({
    url,
    method,
    data: method != "GET" ? params : null,
    params: method == "GET" ? params : null,
    headers: {
      Authorization: `Bearer ${localStorage.token}`,
    },
  }).catch(function (err) {
    //
  });

  let { code, msg, data } = res.data;

  if (code == 401) {
    location.href = "../../html/lj/login.html";
    alert(msg);

    return;
  }

  if (code != 200) {
    alert(msg);
    return null;
  }

  return res;
}
