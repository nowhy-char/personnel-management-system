const mongoose = require("mongoose");

const positionSchema = mongoose.Schema({
    p_id: Number,
    p_name: String,
});

module.exports = mongoose.model("position",positionSchema);