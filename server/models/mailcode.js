const mongoose = require("mongoose");

const mailcodeSchema = mongoose.Schema({
    email: String,
    code: String,
    date: String,
    isLive: String,
});

module.exports = mongoose.model("mailcode",mailcodeSchema);