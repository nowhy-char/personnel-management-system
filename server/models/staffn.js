const mongoose = require("mongoose");

const staffnSchema = mongoose.Schema({
    s_name: String,
    s_card: String,
    sex: Number,
    s_age: String,//年龄
    s_add: String,//地址
    s_phone: Number,//电话
    s_edu: String,//学历
    s_position: Number,//职位
    s_upClock: Number,//上班打卡次数（月初更新）
    s_downClock: Number,//下班打卡次数（月初更新）
    s_kpi: Number,//业绩（每月更新）
});

module.exports = mongoose.model("staffn",staffnSchema);