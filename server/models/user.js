const mongoose = require("mongoose");

const userSchema = mongoose.Schema({
    username: {
        type: String,
        required: [true, "用户名未填写"],
    },
    password: {
        type: String,
        required: [true, "密码未填写"],
        validate: [
            {
              validator: (v) => true,
              msg: "error1",
            },
            {
              validator: (v) => true,
              msg: "error2",
            },
            {
              validator: (v) => true,
              msg: "error3",
            },
            {
              validator: (v) => true,
              msg: "error4",
            },
          ],
    },
    // sex: Number,
    // profile: String,
    message: String,
    email: String,
});

module.exports = mongoose.model("user",userSchema);