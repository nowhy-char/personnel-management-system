const userModel = require("../models/user");
const mailModel = require("../models/mailcode")
const jsonwebtoken = require("jsonwebtoken");
const nodemailer = require("../nodemailer");

//随机数工具方法
function CreateSixNum() {
    let Num = '';
    for(let i = 0; i < 6; i++){
        Num += Math.floor(Math.random()*10);
    }
    return Num;
}

class User {
    async login(req, res) {
        let { username, password } = req.body;
        let user = await userModel.findOne({ username, password });
        if(!user) {
            return res.send({
                code: 400,
                msg: "登陆失败",
                data: null,
            });
        }

        res.send({
            code: 200,
            msg: "ok",
            data: {
                ...user._doc,
                token: jsonwebtoken.sign(
                    { username: user.username},
                    "qwertyuiop"
                ),
            },
        });
    }

    async register(req, res) {
        // 获取数据
        console.log(req.body);
        let { username, password, email, code } = req.body;
        let result = await mailModel.findOne({email, code});
        console.log(result.data);
        var nowDate = (new Date()).getTime();

        if(!result || (new Date(result.date).getTime()) - nowDate > 3600000){
            return res.send({
                code: 400,
                msg: "验证码错误或失效",
                data: null,
            });
        }
    
        let isUsernameExist = await userModel.findOne({ username });
    
        if (isUsernameExist) {
          return res.send({
            code: 400,
            msg: "用户名已被占用",
            data: null,
          });
        }


        await userModel.create({ username, password, email });
    
        res.send({
          code: 200,
          msg: "ok",
          data: null,
        });
    }

    //在注册时，鼠标焦点离开输入框的时候，判断数据库内是否已存在账户
    async findUser(req, res){
        let {username} = req.body;
        let isUsernameExist = await userModel.findOne({ username });

        if (isUsernameExist) {
          return res.send({
            code: 400,
            msg: "用户名已被占用",
            data: null,
          });
        }
    }

    
    //发邮件
    async sendEmail(ctx, res){
        console.log(ctx.body.email);
        let email = ctx.body.email;
        let code = await CreateSixNum();
        let date = new Date();
        console.log(date);
        let isLive = "no";
        
        let mail = {
            from: "2531677517@qq.com",
            subject: "您好",
            to: email,
            text: `${code}为您的验证码，请在1小时内填写`,
        };
        let json = {email,code,date,isLive};
        await mailModel.create(json);
        await nodemailer(mail);

        res.send({
            code: 200,
            msg: "邮件已发送",
            data: null,
        })
    }
}

module.exports = new User();