const userModel = require("../models/staffn");
const jsonwebtoken = require("jsonwebtoken");

class Staffn{

    //分页查询
    async getUsers(req, res) {
      let { limit = 6, offset = 0 } = req.query;

      let count = await userModel.count();
      let page = parseInt(offset / limit) + 1;
      let users = await userModel.find({}).skip(offset).limit(limit).exec();
      return res.send({
        code: 200,
        msg: "ok",
        data: {
          count: count,
          users: users,
          pages: page,
        },
      });
    }


    //查找全部
    async findAllStaffn(req, res){
      let {s_name} = req.query;
      let users = await userModel.find({s_name});
      let count = await users.length;
      return res.send({
        code: 200,
        msg: "ok",
        data: {
          needs: users,
          count: count,
        },
      });
    }


    //职务部门（入职）
    async inWork(req, res) {
      let {s_name,s_position,sex,s_age,s_edu,s_phone,} = req.body;

      if(sex == "男"){
        sex = 1;
      }if(sex == "女"){
        sex = 0;
      }

      let users = await userModel.findOne({s_card});

      let data = {
        "s_name" : s_name,
        "s_position" : s_position,
        "sex" : sex,
        "s_age" : s_age,
        "s_edu" : s_edu,
        "s_phone" : s_phone,
      }

      console.log(data);
      if(users) {
        return res.send({
          code: 400,
          msg: "已存在该员工",
          data: users
        });
      }

      await userModel.create(data);

      return res.send({
        code: 200,
        msg: "ok",
        data: null
      });
    }

    //职务部门（离职）
    async outWork(req, res) {
      let {id} = req.params;

      console.log(id);
      let user =  await userModel.findByIdAndDelete(id.trim());

      if (!user) {
        return res.send({
          code: 400,
          msg: "未找到该员工",
          data: null,
        })
      }

      res.send({
        code: 200,
        msg: "删除成功",
        data: null
      });
    }

    async updateStaffn(req, res) {
      let { s_name } = req.params;
      let { s_kpi } = req.body;
  
      // if (!username && !nickname && !email) {
      //   return res.send({
      //     code: 400,
      //     msg: "修改信息为空",
      //     data: null,
      //   });
      // }
  
      // console.log(id);
  
      // let data = await userModel.findByIdAndUpdate(id, {
      //   username,
      //   nickname,
      //   email,
      // });
  
      await userModel.updateOne(
        { s_name: s_name },
        {
          s_kpi,
        }
      );
  
      res.send({
        code: 200,
        msg: "ok",
        data: s_kpi,
      });
    }
}

module.exports = new Staffn();