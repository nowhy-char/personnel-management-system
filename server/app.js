const express = require("express");
const { expressjwt: jwt } = require("express-jwt");
require("express-async-errors");
const app = express();
const cors = require("cors");
const port = 8080;

// 执行数据库链接
require("./util/db");

// 导入路由模块
const Router = require("./router");

// 配置跨域
app.use(cors());

app.use(
  jwt({ secret: "qwertyuiop", algorithms: ["HS256"] }).unless({
    path: ["/login", "/register", "/sendEmails",/^\/public/],
  })
);

// 解析数据： 解析后的数据会放于 req.body
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

// 使用路由匹配请求
app.use("/", Router);

// 错误处理
app.use((err, req, res, next) => {
  console.log(err.name, err);

  if (err.name === "ValidationError") {
    res.send({
      code: 400,
      msg: err.message.match(/.*?:.*?: (.*)/)[1].replace(/\w*?: /, ""),
      data: null,
    });
    return;
  }

  if (err.name === "UnauthorizedError") {
    res.send({ code: 401, msg: "登陆验证失败", data: null });
  }
  res.send({
    code: 500,
    msg: "server error",
    data: null,
  });
});

app.listen(port, () => {
  console.log(" run in http://localhost:8080");
});
