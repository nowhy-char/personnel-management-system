const mongoose = require("mongoose");
const dbPath = "mongodb://localhost:27017/dbPerson";

mongoose.connect(dbPath);

mongoose.connection.on("open", function () {
  console.log("数据库链接成功");
});

mongoose.connection.on("error", function () {
  console.log("数据库链接失败");
  process.exit();
});
