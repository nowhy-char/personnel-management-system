const express = require("express");
const router = express.Router();

//
const userController = require("./contrllers/user");
const staffnController = require("./contrllers/position")

router
  .post("/login",userController.login)
  .post("/register",userController.register)
  .post("/findUser",userController.findUser)
  .post("/sendEmails",userController.sendEmail)
  .get("/getStaffn",staffnController.getUsers)
  .post("/setStaffn",staffnController.inWork)
  .delete("/setStaffn/:id",staffnController.outWork)
  .patch("/setStaffn/:s_name",staffnController.updateStaffn)
  .get("/search",staffnController.findAllStaffn);

/**
 * 404
 */
router.all("*", (req, res) => {
  res.send({
    code: 404,
    msg: "page not found",
    data: null,
  });
});

module.exports = router;